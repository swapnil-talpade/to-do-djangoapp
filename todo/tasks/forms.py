from django import forms
from django.forms import ModelForm

from .models import *


class TaskForm(forms.ModelForm):
	title= forms.CharField(widget= forms.TextInput(attrs={'placeholder':'Add new task...'}))
	complete = forms.BooleanField(required=False,widget=forms.CheckboxInput(attrs={'class':'checkbox_size'}))

	class Meta:
		model = Task
		fields = '__all__'